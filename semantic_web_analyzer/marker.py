from semantic_web_analyzer import app
from flask import request
from flask import jsonify
from pyld import jsonld
from SPARQLWrapper import SPARQLWrapper, JSON
from flask import request
from flask import jsonify
import json
import re


def formalReplacer(word):
    sparql = SPARQLWrapper("http://35.196.122.28:3030/tesis/query")
    sparql.setQuery(
    """ PREFIX uni: <http://ontologia.peru.upc/dialectos.owl#>
        SELECT *
        WHERE {
               ?dialecto uni:se_dice uni:"""+word+""".
                uni:"""+word+""" uni:palabra_nombre ?significado .
              }
    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    if not results['results']['bindings']:
        results = word
    else :
        results = results['results']['bindings'][0]['significado']['value']

    return results

@app.route("/",methods = ['GET','POST'])
def textReplacer():
    data = request.get_json(force = True)
    key = 'text'

    if(key in data):
        text = data['text']
    else:
        text = data

    words = re.split(r'(;|,|\s)\s*',text)
    print(words)
    result = []

    for word in words :
        print(word)
        if word != ',' and word != '?' and word != '.' and word != '¿' :
            result.append(formalReplacer(word))
        else :
            result.append(word)

    result = ''.join(result)
    print (result)
    return jsonify(result)
