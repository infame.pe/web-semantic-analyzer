from flask import Flask

app = Flask(__name__, instance_relative_config=True)

# Initialize Scripts
import semantic_web_analyzer.marker
