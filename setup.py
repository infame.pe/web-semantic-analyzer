from setuptools import setup

setup(
    name='semantic_web_analyzer',
    packages=['semantic_web_analyzer'],
    include_package_data=True,
    install_requires=[
        'flask',
        'PyLD',
        'sparqlwrapper'
    ],
)
